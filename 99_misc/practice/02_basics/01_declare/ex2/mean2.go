package main

import "fmt"

func main() {
	// this is short declaration
	x := 1
	y := 2

	fmt.Printf("x=%v, type of %T\n", x, x)
	fmt.Printf("x=%v, type of %T\n", y, y)

	mean := (x + y) / 2.0
	fmt.Printf("mean=%v, type of %T\n", mean, mean)

}

// run with terminal command ==> go run mean1.go
