package main

import (
	"fmt"
)

func main() {
	var x int // => int, uint
	var y int // int8,int16,int32,int64
	// uint8, uint16,uint32,uint64

	x = 1
	y = 2

	fmt.Printf("x=%v, type of %T\n", x, x)
	fmt.Printf("y=%v, type of %T\n", y, y)

	mean := (x + y) / 2
	fmt.Printf("mean=%v, type of %T\n", mean, mean)
	// when you divide int with int ==>> int
}

// run with terminal command ==> go run mean1.go
