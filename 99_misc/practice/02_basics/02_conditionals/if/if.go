package main

import (
	"fmt"
)

func main() {
	x := 10

	if x > 5 {
		fmt.Printf("%v is big\n", x)
	}
	//  operators < > == => <=
	// Logical operators && ||
	if x > 100 {
		fmt.Printf("%v is big\n", x)
	} else {
		fmt.Printf("%v is not big then %v\n", x, 100)
	}

	if x > 5 && x < 15 {
		fmt.Printf("%v is  just right\n", x)
	}

	if x < 20 || x > 30 {
		fmt.Printf("%x is out of range\n", x)
	}
	a := 11.0
	b := 20.0

	if frac := a / b; frac > 0.5 {
		fmt.Printf("%v is more then 0.5\n", frac)
	}
}
