package main

import (
	"fmt"
)

func main() {
	n := 2

	switch n {
	case 1:
		fmt.Printf("%v\n", n)
	case 2:
		fmt.Printf("%v\n", n)
	case 3:
		fmt.Printf("%v\n", n)
	case 4:
		fmt.Printf("%v\n", n)
	default:
		fmt.Printf("%v was provided\n", n)

	}

	switch {
	case n > 100:
		fmt.Printf("n is very big\n")
	case n < 10:
		fmt.Printf("n is big\n")
	default:
		fmt.Printf("n is small\n")
	}
}
