# Go Essential Training

Go Lang Programming Course Led by Miki Tebeka.
This repo is just a note-summary for myself.

[Init](./00_init/README.md)
[Setup](./01_setup/README.md)
[Basics](./02_basics/README.md)
[Functions](./03_functions/README.md)
[Oop](./04_oop/README.md)
[Error_handle](./05_error_handle/README.md)
[Concurrency](./06_concurency/README.md)
[Project management](./07_project_mngmt/README.md) 
[Network](./08_network/README.md)
[Miscellaneous](./99_misc/README.md)

